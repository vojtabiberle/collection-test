<?php

declare(strict_types=1);

use Collections\Collection;
use Collections\CollectionInterface;

/**
 * @phpstan-template C
 * @phpstan-template T
 *
 * Class ProxyCollection
 */
abstract class ProxyCollection implements CollectionInterface
{
    /** @var CollectionInterface<T> */
    private $collection;

    /**
     * @param string $type
     * @param array  $items
     * @phpstan-param class-string<C>|string $type
     * @phpstan-param T[] $items
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function __construct(string $type, array $items = [])
    {
        $this->collection = new Collection($type, $items);
    }

    public function getType(): string
    {
        return $this->collection->getType();
    }

    /**
     * @phpstan-param T $item
     *
     * @return CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function add($item)
    {
        $this->collection = $this->collection->add($item);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @return self|CollectionInterface
     * @phpstan-return C
     */
    public function clear()
    {
        $this->collection = $this->collection->clear();

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $condition
     *
     * @return bool
     */
    public function contains(callable $condition)
    {
        return $this->collection->contains($condition);
    }

    /**
     * @param callable $condition
     *
     * @return bool|mixed
     * @phpstan-return bool|T
     */
    public function find(callable $condition)
    {
        return $this->collection->find($condition);
    }

    /**
     * @param callable $condition
     *
     * @return int
     */
    public function findIndex(callable $condition)
    {
        return $this->collection->findIndex($condition);
    }

    /**
     * @param int $index
     *
     * @return mixed
     * @phpstan-return T
     *
     * @throws \Collections\Exceptions\OutOfRangeException
     */
    public function at($index)
    {
        return $this->collection->at($index);
    }

    /**
     * @param int $index
     *
     * @return bool
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function indexExists($index)
    {
        return $this->collection->indexExists($index);
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->collection->count();
    }

    /**
     * @param callable $condition
     *
     * @return $this|CollectionInterface
     * @phpstan-return C
     */
    public function filter(callable $condition)
    {
        $this->collection = $this->collection->filter($condition);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $condition
     *
     * @return bool|mixed
     * @phpstan-return bool|T
     */
    public function findLast(callable $condition)
    {
        return $this->collection->findLast($condition);
    }

    /**
     * @param callable $condition
     *
     * @return int
     */
    public function findLastIndex(callable $condition)
    {
        return $this->collection->findLastIndex($condition);
    }

    /**
     * @return \ArrayIterator
     * @phpstan-return \Iterator<T>
     */
    public function getIterator()
    {
        return $this->collection->getIterator();
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return self|CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function slice($start, $end)
    {
        $this->collection = $this->collection->slice($start, $end);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int   $index
     * @param array $item
     * @phpstan-param T $item
     *
     * @return self|CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     * @throws \Collections\Exceptions\OutOfRangeException
     */
    public function insert($index, $item)
    {
        $this->collection = $this->collection->insert($index, $item);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int   $index
     * @param array<T> $items
     * @phpstan-params T[] $items
     *
     * @return self|CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\OutOfRangeException
     */
    public function insertRange($index, array $items)
    {
        $this->collection = $this->collection->insertRange($index, $items);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $condition
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     */
    public function without(callable $condition)
    {
        $this->collection = $this->collection->without($condition);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int $index
     *
     * @return self|CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\OutOfRangeException
     */
    public function removeAt($index)
    {
        $this->collection = $this->collection->removeAt($index);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @return self|CollectionInterface
     * @phpstan-return C
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function reverse()
    {
        $this->collection = $this->collection->reverse();

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $callback
     *
     * @return self|CollectionInterface
     * @phpstan-return C
     */
    public function sort(callable $callback)
    {
        $this->collection = $this->collection->sort($callback);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @return array
     * @phpstan-return T[]
     */
    public function toArray()
    {
        return $this->collection->toArray();
    }

    /**
     * @param callable   $callable
     * @param void|mixed $initial
     *
     * @return mixed
     */
    public function reduce(callable $callable, $initial = null)
    {
        return $this->collection->reduce($callable, $initial);
    }

    /**
     * @param callable $condition
     *
     * @return bool
     */
    public function every(callable $condition)
    {
        return $this->collection->every($condition);
    }

    /**
     * @param int $num
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function drop($num)
    {
        $this->collection = $this->collection->drop($num);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int $num
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function dropRight($num)
    {
        $this->collection = $this->collection->dropRight($num);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $condition
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     */
    public function dropWhile(callable $condition)
    {
        $this->collection = $this->collection->dropWhile($condition);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function tail()
    {
        $this->collection = $this->collection->tail();

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int $num
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function take($num)
    {
        $this->collection = $this->collection->take($num);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param int $num
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function takeRight($num)
    {
        $this->collection = $this->collection->takeRight($num);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $condition
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     */
    public function takeWhile(callable $condition)
    {
        $this->collection = $this->collection->takeWhile($condition);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $callable
     */
    public function each(callable $callable): void
    {
        $this->collection->each($callable);
    }

    /**
     * @param callable $callable
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     */
    public function map(callable $callable)
    {
        $this->collection = $this->collection->map($callable);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param callable $callable
     * @param null     $initial
     *
     * @return mixed
     */
    public function reduceRight(callable $callable, $initial = null)
    {
        return $this->collection->reduceRight($callable, $initial);
    }

    /**
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     */
    public function shuffle()
    {
        $this->collection = $this->collection->shuffle();

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @param array $items
     * @phpstan-param T[] $items
     *
     * @return self|CollectionInterface
     * @phpstan-return C|Collection<T>
     *
     * @throws \Collections\Exceptions\InvalidArgumentException
     */
    public function merge($items)
    {
        $this->collection = $this->collection->merge($items);

        /** @phpstan-var C $this; */
        return $this;
    }

    /**
     * @return mixed
     * @phpstan-return T
     */
    public function first()
    {
        return $this->collection->first();
    }

    /**
     * @return mixed
     * @phpstan-return T
     */
    public function last()
    {
        return $this->collection->last();
    }

    /**
     * @return array
     * @phpstan-return T[]
     */
    public function hat()
    {
        return $this->collection->hat();
    }

    /**
     * @return array
     * @phpstan-return T[]
     */
    public function headAndTail()
    {
        return $this->collection->headAndTail();
    }

    /**
     * @return array|mixed
     * @phpstan-return T[]
     */
    public function popPop()
    {
        return $this->collection->popPop();
    }
}
