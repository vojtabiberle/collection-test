<?php

declare(strict_types=1);


/**
 * @phpstan-extends ProxyCollection<ItemCollection, Item>
 */
class ItemCollection extends ProxyCollection 
{
    /**
     * @param array<Item> $items
     */
    public function __construct(array $items = [])
    {
        parent::__construct(Item::class, $items);
    }
}